// index.js

import LightBlockchain from './lightblockchain.js';
import Block from './block.js';

export default LightBlockchain;
export { Block };