// lightblockchain.js

import Block from './block.js';

/**
 * Class LightBlockchain
 */
class LightBlockchain {

    constructor() {
        // Create an array with an initial block
        this.chain = new Array(new Block({}));
    }

    addBlock(block) {
        // Get the hash from the previous block
        block.previous = this.getLastBlock().hash;
        // Hash the new block with the updated previous attribute
        block.hash = block.hashBlock();
        // Push the block to the chain
        this.chain.push(block)
    }

    getLastBlock() {
        // Return the last block of the chain
        return this.chain[this.chain.length -1];
    }

    validate() {
        return !this.chain
            .slice(1, this.chain.length - 1)
            .some((currentBlock, index) => {
                // Is the hash matching the current state of the block ?
                if (currentBlock.hash !== currentBlock.hashBlock()) {
                    return true;
                }

                // Is the previous block hash valid ?
                if (currentBlock.previous !== this.chain[index].hash) {
                    return true;
                }

                return false
            });
    }
}

export default LightBlockchain;