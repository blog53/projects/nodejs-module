// block.js

import crypto from 'crypto';

class Block {

    constructor(data, previous = "") {
        this.timestamp = Date.now();
        this.data = data;
        this.previous = previous;
        this.hash = this.hashBlock();
    }

    hashBlock() {
        // Build the string used for hash generation
        let blockString = `${this.previous}-${this.timestamp}-${JSON.stringify(this.data)}`;
        // Hash the string using SHA256 algorithm and return it
        return crypto.createHash('sha256').update(blockString).digest('hex');
    }

}

export default Block;