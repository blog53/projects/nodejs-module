import LightBlockchain, { Block } from 'light-blockchain';

const invoice1 = {
    emiter: 'bakery',
    amount: 10.5,
    method: 'card'
};

const invoice2 = {
    emiter: 'sushi',
    amount: 25,
    method: 'cash'
};

const blockchain = new LightBlockchain();

// Ajout des utilisateurs à la blockchain
blockchain.addBlock(new Block(invoice1));
blockchain.addBlock(new Block(invoice2));

// Vérification de la validité de la blockchain
console.log(`The blockchain is${blockchain.validate() ? '': 'not'} valid`);

// Altération de la blockchain
blockchain.chain[1].data.amount = 30;

// Vérification de la validité de la blockchain
console.log(`The blockchain is${blockchain.validate() ? '': ' not'} valid`);

